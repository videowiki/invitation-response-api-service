const { server, app } = require('./generateServer')();
const controller = require('./controller');

app.get('/health', (req, res) => {
    return res.status(200).send('OK')
})

app.all('*', (req, res, next) => {
    if (req.headers['vw-user-data']) {
        try {
            const user = JSON.parse(req.headers['vw-user-data']);
            req.user = user;
        } catch (e) {
            console.log(e);
        }
    }
    next();
})

app.post('/organization/:organizationId/invitations/respond', controller.respondToOrganizationInvitation)
app.post('/article/:articleId/translators/invitation/respond', controller.updateTranslatorInvitation)
app.post('/article/:articleId/textTranslators/invitation/respond', controller.updateTextTranslatorInvitation)

const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
